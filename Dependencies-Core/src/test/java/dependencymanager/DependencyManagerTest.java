package dependencymanager;

import com.felixcool98.dependencies.Dependency;
import com.felixcool98.dependencies.DependencyManager;

public class DependencyManagerTest {
	public static void main(String[] args) {
		DependencyManager<String> manager = new DependencyManager<String>();
		
		Dependency<String> recipe = manager.wrap("recipe", "recipe");
		
		Dependency<String> item = manager.wrap("item", "item");
		recipe.add("item");
		
		Dependency<String> block = manager.wrap("block", "block");
		item.add("block");
		block.add("recipe");
		
		Dependency<String> undefined = manager.wrap("undefined", "undefined");
		undefined.add("random");
		
		for(String str : manager.getObjects()) {
			System.out.println(str);
		}
	}
}
