package com.felixcool98.dependencies;

import java.util.ArrayList;
import java.util.List;

public class CircleExeption extends RuntimeException {
	private static final long serialVersionUID = -4847926212082979249L;

	
	private List<String> chain = new ArrayList<String>();
	
	
	private CircleExeption(String msg) {
		super(msg);
	}
	
	
	public static CircleExeption get(CircleExeption e, String a) {
		List<String> chain = e.chain;
		
		chain.add(a);
		
		String msg = toString(chain);
		
		CircleExeption exeption = new CircleExeption(msg);
		
		exeption.chain = chain;
		
		return exeption;
	}
	public static CircleExeption get(String a, String b) {
		List<String> chain = new ArrayList<String>();
		
		chain.add(a);
		chain.add(b);
		
		String msg = toString(chain);
		
		CircleExeption exeption = new CircleExeption(msg);
		
		exeption.chain = chain;
		
		return exeption;
	}
	private static String toString(List<String> chain) {
		String msg = "";
		
		if(!chain.isEmpty()) {
			for(String dependency : chain) {
				msg += dependency.toString()+" < ";
			}
			msg = msg.substring(0, msg.length()-3);
		}
		
		return msg;
	}
}
