package com.felixcool98.dependencies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DependencyManager<T> {
	private Map<String, Dependency<T>> dependencies = new HashMap<String, Dependency<T>>();
	private List<Dependency<T>> failed = new ArrayList<Dependency<T>>();
	
	private boolean verified = false;
	
	
	public Dependency<T> wrap(String name, T object){
		Dependency<T> dependency = new Dependency<T>(name, object);
		
		dependencies.put(dependency.getName(), dependency);
		
		verified = false;
		
		return dependency;
	}
	
	
	public Dependency<T> get(String name){
		return dependencies.get(name);
	}
	
	
	public List<Dependency<T>> getAll(){
		verify();
		
		return this.dependencies.values().stream().sorted().map(t ->{
			t.checkForCircle(this);
			
			return t;
		})
		.collect(Collectors.toList());
	}
	public List<T> getObjects(){
		return getAll().stream()
				.map(Dependency::getObject)
				.collect(Collectors.toList());
	}
	
	
	public void verify() {
		if(verified)
			return;
		
		for(Dependency<T> dependency : dependencies.values().stream().sorted().collect(Collectors.toList())) {
			verify(dependency);
		}
		
		verified = true;
	}
	public void verify(Dependency<T> dependency) {
		boolean remove = false;
		
		for(String name : dependency.getDependencies()) {
			if(!dependencies.containsKey(name)) {
				System.err.println("couldn't resolve "+name+" is it not registered?");
				
				remove = true;
			}
		}
		
		if(remove) {
			dependencies.remove(dependency.getName());
			failed.add(dependency);
			System.err.println("removed "+dependency+" because some dependencies couldn't be resolved");
		}
	}
	public List<Dependency<T>> getFailed() {
		return failed;
	}
}
