package com.felixcool98.dependencies;

import java.util.LinkedList;
import java.util.List;

public class Dependency<T> implements Comparable<Dependency<T>> {
	private T object;
	
	private String name;
	
	private List<String> dependencies = new LinkedList<String>();
	
	
	public Dependency(String name, T object) {
		this.name = name;
		this.object = object;
	}
	
	
	public void add(Dependency<?> dependency) {
		add(dependency.getName());
	}
	public void addAll(String[] dependencies) {
		for(String dependency : dependencies) {
			add(dependency);
		}
	}
	public void add(String dependency) {
		dependencies.add(dependency);
	}
	
	public List<String> getDependencies() {
		return dependencies;
	}
	
	public boolean dependsOn(Dependency<?> dependency) {
		return dependsOn(dependency.getName());
	}
	public boolean dependsOn(String dependency) {
		for(String name : getDependencies()) {
			if(name.equals(dependency))
				return true;
		}
		
		return false;
	}
	
	public boolean hasDependencies() {
		return !dependencies.isEmpty();
	}
	
	public String getName() {
		return name;
	}
	public T getObject() {
		return object;
	}
	
	@Override
	public String toString() {
		return getObject().toString();
	}


	@Override
	public int compareTo(Dependency<T> o) {
		if(!this.hasDependencies() && !o.hasDependencies())
			return 0;
		if(!this.hasDependencies() && o.hasDependencies())
			return -1;
		if(this.hasDependencies() && !o.hasDependencies())
			return 1;
		
		if(this.dependsOn(o)) {
			if(o.dependsOn(this))
				throw new IllegalArgumentException("circle between "+this+" and "+o);
			
			return 1;
		}else 
			return -1;
	}
	public void checkForCircle(DependencyManager<T> manager) {
		checkForCircle(this, manager);
	}
	private void checkForCircle(Dependency<?> src, DependencyManager<T> manager) {
		for(String d : getDependencies()) {
			if(d.equals(src.getName()))
				throw CircleExeption.get(d, this.getName());
			
			if(manager.get(d).hasDependencies()) {
				try {
					manager.get(d).checkForCircle(src, manager);
				}catch (CircleExeption e) {
					throw CircleExeption.get(e, this.getName());
				}
			}
		}
	}
}
